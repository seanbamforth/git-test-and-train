# git-test-and-train

Git project for testing and training in Git & GitLab
 - make changes
 - commit changes to your local repo
 - push them to origin (gitlab, github)

 Branches 
  - create a new branch with
  - Check that branch out 
  - Make your changes
  - Commit changes
  - Push them to origin (if required)

Difference between commiting and pushing 
  - Commit to save changes locally
  - Push to send them to Gitlab